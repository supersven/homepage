---
author: thoughtpolice
title: "GHC Weekly News - 2014/11/07"
date: 2014-11-07
tags: ghc news
---

Hello \*,

It's that time again, so get ready for some good ol' fashion news about your favorite compiler.

  - Austin announced the 7.10 STABLE freeze date earlier today, which is two
  weeks from the time of this posting: November 21st, 2014. If you're a
  developer and have a feature you want, you'd better get it reviewed and
  checked soon!
  <https://www.haskell.org/pipermail/ghc-devs/2014-November/007206.html>

  - Austin also opened a discussion about a possible LTS branch for GHC,
  spawned off from a suggestion by John Lato a few weeks email. This discussion
  has been brought up several times before this, but for the most part has
  fizzled out a bit. But maybe with a different focus - on a separate branch
  with a team of maintainers - we can hash out a plan of action, and just give
  it a whirl.
  <https://www.haskell.org/pipermail/ghc-devs/2014-November/007207.html>

  - This past week, Simon PJ, Austin, Gintautas Miliauskas, and several others
  met over a video chat to discuss the future of windows builds. And it went
  pretty well! We've scribed up some notes, and sort of laid out what we think
  will be happening for Windows in the near future.
  <https://www.haskell.org/pipermail/ghc-devs/2014-October/006897.html>

  - Gergo Erdi opened up an RFC about type signatures for pattern synonyms,
  which is one of the last pieces of the pattern synonyms implementation we've
  been missing.
  <https://www.haskell.org/pipermail/ghc-devs/2014-November/007066.html>

  - Simon PJ pushed a major overhaul of the constraint solver, just in time for
  GHC 7.10. This was the result of a few months of work that Simon has been
  glad to get off his plate, and hopefully should make the type checker faster,
  leaner, and more modular (as usual).

  - Jan Stolarek talked about his planned improvements to the users guide,
  which is ticket #9358. Hopefully for 7.10 the resulting documentation will be
  *much* more clear and up to date.
  <https://www.haskell.org/pipermail/ghc-devs/2014-November/007169.html>

  - Alan Zimmerman has got some more patches up for adding annotations to the
  GHC Abstract Syntax Tree (AST). The hope is this new representation will make
  it much easier for tools to enrich the AST with their own custom metadata.
  Alan has been working on this for several weeks now, so a good review is in
  order! <https://www.haskell.org/pipermail/ghc-devs/2014-November/007133.html>

  - Mateusz Lenik, a new contributor, has discussed improving the 'Ticky Ticky'
  profiling code and resurrecting some of the older features; luckily Jan
  inspired this work and had some comments. Thanks Mateusz!
  <https://www.haskell.org/pipermail/ghc-devs/2014-November/007078.html>

  - Alexander Berntsen asked an question about abstracting over constraints in
  GHC. Richard replied, but it seems this work might be quite difficult!
  <https://www.haskell.org/pipermail/ghc-devs/2014-November/007165.html>

  - Austin Seipp brought up a question about Windows support: can we officially
  drop support for XP, now that Microsoft has done the same? And what minimum
  version requirements should we endorse? Vista or Windows 7 would give
  improvements due to API improvements, with Windows 7 offering even more. If
  you're a GHC on Windows user, please let us know!
  <https://www.haskell.org/pipermail/ghc-devs/2014-November/007199.html>

And this weeks closed tickets include quite a long list, thanks to everyone cleaning up the bug tracker: #9747, #9236, #9753, #9752, #9262, #8953, #9084, #9738, #8571, #8295, #8261, #9754, #110, #9345, #8849, #8819, #9658, #8960, #9395, #9705, #9433, #9633, #9359, #9081, #8482, #3376, #9712, #9739, #9211, #9728, #9750, #9768, #9773, #9741, #9284, #9774, #9771, #9001, #8626, #8986, #9268, #8975, #8962, #8921, #8089, #8843, #8829, #9295, #7913, #2528, #9779.
