---
author: hvr
title: "Haskell Implementors Workshop 2014 videos available!"
date: 2014-09-08
tags: 
---

Without further ado, here's the [HIW 2014 Youtube Playlist](http://www.youtube.com/playlist?list=PL4UWOFngo5DW6nKDjK0UB5Oy9zmdWdo7K) (kindly provided by Malcolm Wallace)
