---
author: bgamari
title: "GHC 8.8.1-rc1 released"
date: 2019-07-22
tags: release
---

The GHC team is pleased to announce the second and likely last alpha
release of GHC 8.8.1. The source distribution, binary distributions, and
documentation are available from
[downloads.haskell.org](https://downloads.haskell.org/~ghc/8.8.1-rc1).
A draft of the release notes is also [available][1].

This release is the culmination of over 3000 commits by over one hundred
contributors and has several new features and numerous bug fixes
relative to GHC 8.6:

 * Profiling now works correctly on 64-bit Windows (although still may
   be problematic on 32-bit Windows due to platform limitations; see
   #15934)

 * A new code layout algorithm for amd64's native code generator

 * The introduction of a late lambda-lifting pass which may reduce
   allocations significantly for some programs.

 * Further work on Trees That Grow, enabling improved code re-use of the
   Haskell AST in tooling

 * More locations where users can write `forall` ([GHC Proposal #0007][proposal7])

 * A comprehensive audit of GHC's memory ordering barriers has been
   performed, resulting in a number of fixes that should significantly
   improve the reliability of programs on architectures with
   weakly-ordered memory models (e.g. PowerPC, many ARM and AArch64
   implementations).

 * A long-standing linker limitation rendering GHCi unusable with
   projects with cyclic symbol dependencies has been fixed (#13786)

 * Further work on the Hadrian build system

 * Numerous bug-fixes

As always, if anything looks amiss do let us know.

Happy compiling!




[1]: https://downloads.haskell.org/ghc/8.8.1-alpha2/docs/html/users_guide/8.8.1-notes.html
[proposal7]: https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0007-instance-foralls.rst

