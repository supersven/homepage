---
author: thoughtpolice
title: "GHC Weekly News - 2015/06/03"
date: 2015-06-03
tags: ghc news
---

Hi \*,

It's that time once again - to get some info on what's happening in the world of GHC! It's been a quiet few weeks as a UK Holiday punted one of GHC HQ's meetings off, and this week we were only partially there.

The main point of discussion was 7.10.2, and continuing work on compiler performance. The good news is, the past few weeks have seen good work on both these fronts!

## 7.10.2 status

7.10.2 is swimming along very nicely - the [status page](https://ghc.haskell.org/trac/ghc/wiki/Status/GHC-7.10.2) shows the current set of tickets we've fixed and plan on fixing.

Not much has changed from last time, except we've fixed even more bugs! We're currently sitting at about 85 bugs fixed, some of them pretty important - code generation bugs, compiler performance fixes, some RTS and event manager work. Your author is actually quite happy with what GHC 7.10.2 looks like, at this rate.

## List chatter

  - Austin Seipp announced that GHC 7.10.2 will be release soon, and developers/users should get bugs they want fixed reported to us ASAP so we can do something. https://mail.haskell.org/pipermail/ghc-devs/2015-June/009150.html

  - Mark Lentczner announced a Haskell Platform alpha featuring GHC 7.10.2 https://mail.haskell.org/pipermail/ghc-devs/2015-June/009128.html

  - Facundo Dominguez asks: sometimes we want to create a `static` pointer in a function with a local definition, how can we do that? The current problem is the desugarer gets in the way and current approaches are currently rejected, but Facundo has some ideas/questions about a fix. https://mail.haskell.org/pipermail/ghc-devs/2015-May/009110.html

  - David Macek has made great progress on getting native MSYS2 packages for windows working - which should be a great boon to all our Windows users! https://mail.haskell.org/pipermail/ghc-devs/2015-May/009089.html

  - Joachim Breitner announced the new GHC performance dashboard, which can be used to track all of GHC's performance-based tests over time. Whoohoo! https://mail.haskell.org/pipermail/ghc-devs/2015-May/009032.html

  - Joachim Breitner asked: is there a way to programmatically 'Raise a Concern' on a Phabricator commit? With the new https://perf.haskell.org/ghc/ work, it'd be nice if regressions could be automatically flagged. The current problem is there is no API endpoint, but one can be built. https://mail.haskell.org/pipermail/ghc-devs/2015-June/009128.html

  - Adam Gundry asked ghc-devs about some input on changes to the new typechecker plugins API. After some discussion and elbow grease, the new changes have already landed in HEAD and will be in 7.12.1. https://mail.haskell.org/pipermail/ghc-devs/2015-May/009097.html

## Noteworthy commits

  - Commit 45d9a15c4b85a2ed89579106bdafd84accf2cb39 - Fix a huge space leak in the mighty simplifier

  - Commit c89bd681d34d3339771ebdde8aa468b1d9ab042b - Fix quadratic behavior in `tidyOccName`

  - Commit b03f074fd51adfb9bc4f5275294712ee62741aed - ghci: Allow `:back` and `:forward` to take counts

  - Commit 8e4dc8fb63b8d3bfee485c1c830776f3ed704f4d - Greatly speed up `nativeCodeGen/seqBlocks` 

  - Commit c256357242ee2dd282fd0516260edccbb7617244 - Speed up `elimCommonBlocks` by grouping blocks also by outgoing labels

  - Commit f5188f3acd73a07b648924a58b9882c2d0a3dbcb - Fix weird behavior of `-ignore-dot-ghci` and `-ghci-script`

  - Commit 4fffbc34c024231c3c9fac7a2134896cc09c7fb7 - New handling of overlapping instances in Safe Haskell

  - Commit f16ddcee0c64a92ab911a7841a8cf64e3ac671fd - Support stage 1 Template Haskell (non-quasi) quotes, fixes #10382

  - Commit cf7573b8207bbb17c58612f3345e0b17d74cfb58 - More accurate allocation stats for `:set -s`

## Closed tickets

\#10407, #10408, #10177, #10359, #10403, #10248, #9579, #10415, #10419, #10427, #10429, #10397, #10422, #10335, #10366, #10110, #10397, #10349, #10244, #8555, #8799, #9131, #10396, #10354, #10278, #9899, #3533, #9950, #10092, #9950, #10430, #9682, #9584, #10446, #10410, #10298, #10449, #10399, #7695, #10261, #8292, #10360, #10126, #10317, #10101, #10322, #10313, #10471, #10473, #7170, #10473, #10423, #10466, #8695, #10461, #10052, #10370, #10425, #10452, #10474, 
